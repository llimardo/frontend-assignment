// React
import { useState } from 'react';

// Styles
import './App.css';

// Images
import logo from './img/logo.png';
import pikachu from './img/detective-pikachu.png';

// Apollo
import ApolloClient from 'apollo-boost';
import { gql } from "apollo-boost";

// Antd
import { Radio, Input, Space, Select, Table, Tag, Button, Modal } from 'antd';
import { RightOutlined } from '@ant-design/icons';
import 'antd/dist/antd.css';
const { Search } = Input;
const { Option } = Select;
const { Column, ColumnGroup } = Table;

interface PokemonInterface {
  id: string,
  name: string,
  types: string[]
}

const client = new ApolloClient({
  uri: 'http://localhost:4000',
});

function App() {
  // Filter Types
  const name = "Name";
  const type = "Type";

  const pokemonTypes = ["Fire", "Water", "Electric", "Grass", "Fighting", "Fairy", "Poison", "Ground", "Rock", "Bug", "Ghost", "Dragon", "Ice", "Flying", "Steel", "Normal", "Psychic"];
  const valuesNPerPage = [1, 5, 10, 15, 20, 25, 50];

  const [pokemons, setPokemons] = useState<PokemonInterface[]>([]);
  const [filterShow, setFilterShow] = useState<string>(name);
  const [index, setIndex] = useState<number>(1);

  // Filter by Name variables
  const [filterName, setFilterName] = useState<string>("");
  const [nPerPage, setNPerPage] = useState<number>(valuesNPerPage[1]);
  const [hasNextPage, setHasNextPage] = useState<boolean>(false);
  const [endCursor, setEndCursor] = useState<string>("");

  function changeFilterType(value: string = "") {
    setFilterShow(value);
    setFilterName("");
    setHasNextPage(false);
    setPokemons([]);
    setIndex(1);
  }

  function filterByName(q: string = "", limit: number = 5, after: string = "000") {
    client
      .query({
        query: gql`
          {
            pokemons(q: "${q}", limit: ${limit}, after: "${after}") {
              edges {
                cursor 
                node {
                  id
                  name
                  types
                }
              }
              pageInfo {
                endCursor
                hasNextPage
              }
            }
          }
        `
      })
      .then(result => {
        formatResult(result.data.pokemons.edges);
        setEndCursor(result.data.pokemons.pageInfo.endCursor);
        setHasNextPage(result.data.pokemons.pageInfo.hasNextPage);
        if (after !== "000") setIndex(index + 1);
        else setIndex(1);
      })
      .catch(error => showErrorMessage(error));
  }

  function filterByType(type: string = "") {
    client
      .query({
        query: gql`
          {
            pokemonsByType(type: "${type}") {
              edges {
                cursor 
                node {
                  id
                  name
                  types
                }
              }
            }
          }
        `
      })
      .then(result => formatResult(result.data.pokemonsByType.edges))
      .catch(error => showErrorMessage(error));
  }

  function formatResult(result: any = []) {
    const formatResult = [];
    for (const element of result) {
      formatResult.push({ ...element.node, key: element.node.id });
    }
    setPokemons(formatResult);
  }

  function showErrorMessage(error: any = {}) {
    Modal.error({
      title: error?.networkError?.statusCode ? `${error?.networkError?.name} (${error?.networkError?.statusCode})` : "Error",
      content: error?.networkError?.result ? error?.networkError?.result?.errors[0]?.message : error?.message,
    })
  }

  function setColorType(type: string = "") {
    switch (type) {
      case "Grass":
      case "Bug": return "green";
      case "Electric": return "warning";
      case "Fire": return "red";
      case "Water": return "blue";
      case "Psychic":
      case "Poison": return "purple";
      case "Rock": return "grey";
      case "Dragon": return "volcano";
      case "Flying": return "processing";
      case "Ice": return "geekblue";
      case "Ground": return "brown";
      case "Fighting": return "magenta";
      default: return "default";
    }
  }

  return (
    <Space direction="vertical" className="App">
      <div className="logo-container">
        <img src={logo} className="logo" alt="logo" />
      </div>
      <div>
        <h3 className="filter-title">Filter by:</h3>
        <Radio.Group name="filterShow" defaultValue={name} onChange={evt => changeFilterType(evt.target.value)}>
          <Radio value={name}>{name}</Radio>
          <Radio value={type}>{type}</Radio>
        </Radio.Group>
      </div>

      <div className="filter-container">
        {filterShow === name &&
          <Search onChange={evt => setFilterName(evt.target.value)} value={filterName} onSearch={value => filterByName(value, nPerPage)} enterButton />
        }
        {filterShow === type &&
          <Select
            defaultValue=""
            style={{ width: 250 }}
            showSearch
            placeholder="Search to Select"
            onChange={value => filterByType(value)}
            filterOption={(input, option: any) =>
              option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
            filterSort={(optionA: any, optionB: any) =>
              optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
            }
          >
            {pokemonTypes.sort().map((pokemonType, index) => <Option key={index} value={pokemonType}>{pokemonType}</Option>)}
          </Select>
        }
      </div>

      <Table
        dataSource={pokemons}
        className="table"
        indentSize={pokemons.length}
        pagination={false}
        scroll={{ y: 310 }}
        locale={{
          emptyText: (<span>
            <img src={pikachu} width={300} alt="pokémon not found" />
            <div className="empty-text">No Pokémons found</div>
          </span>)
        }}
      >
        <ColumnGroup title={`Pokédex ${filterShow === name && pokemons.length > 0 ? `(page ${index})` : ""}`}>
          <Column title="ID" dataIndex="id" key="id" />
          <Column title="Name" dataIndex="name" key="name" />
          <Column
            title="Types"
            dataIndex="types"
            key="types"
            render={types => (
              <>
                {types.map((type: string) => (
                  <Tag color={setColorType(type)} key={type}>
                    {type}
                  </Tag>
                ))}
              </>
            )}
          />
        </ColumnGroup>
      </Table>

      {filterShow === name && <Space direction="horizontal" className="bottom">
        <Select
          className="select-n-per-page"
          onChange={value => {
            setNPerPage(value);
            filterByName(filterName, value);
          }}
          value={nPerPage}
          disabled={pokemons.length === 0}
        >
          {valuesNPerPage.map((valueNPerPage, index) => <Option key={index} value={valueNPerPage}>N Pokémons per Page: {valueNPerPage}</Option>)}
        </Select>
        <Button type="primary" disabled={!hasNextPage} onClick={() => filterByName(filterName, nPerPage, endCursor)}>Next <RightOutlined /></Button>
      </Space>}
    </Space>
  );
}

export default App;

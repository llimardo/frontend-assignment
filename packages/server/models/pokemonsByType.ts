import { pipe } from "fp-ts/lib/pipeable";
import * as O from "fp-ts/lib/Option";
import * as A from "fp-ts/lib/Array";
import { identity } from "fp-ts/lib/function";
import { data } from "../data/pokemons";
import { toConnection, slice } from "../functions";
import { Connection } from "../types";

interface Pokemon {
  id: string;
  name: string;
  types: string[];
}

export function queryByType(args: {
  type?: string;
}): Connection<Pokemon> {
  const { type } = args;

  const filterByType: (as: Pokemon[]) => Pokemon[] =
    // filter only if q is defined
    type === undefined
      ? identity
      : A.filter(p => type ? p.types.includes(type.charAt(0).toUpperCase() + type.slice(1).toLocaleLowerCase()) : true);

  const results: Pokemon[] = pipe(
    data,
    filterByType,
  );
  return toConnection(results, results.length);
}